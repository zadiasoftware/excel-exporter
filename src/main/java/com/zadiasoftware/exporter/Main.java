package com.zadiasoftware.exporter;

import com.zadiasoftware.exporter.exporters.ImageExporter;
import com.zadiasoftware.exporter.models.Image;
import com.zadiasoftware.exporter.utils.excel.export.ExportUtil;

import java.util.List;

public class Main {

    private static ImageExporter imageExporter = new ImageExporter();

    public static void main(String[] args) {
        List<Image> images = imageExporter.getAllImages();
        ExportUtil.export(images);
    }
}
