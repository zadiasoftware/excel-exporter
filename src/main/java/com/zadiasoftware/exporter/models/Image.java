package com.zadiasoftware.exporter.models;

import com.zadiasoftware.exporter.utils.excel.export.Exportable;

public class Image implements Exportable {

    private String propertyId;
    private String url;
    private String fileName;
    private String title;
    private String sortOnWebsite;
    private String sortOnExpose;
    private String sortOnPortal;
    private String website;
    private String portal;
    private String expose;
    private String tags;

    public String getPropertyId() {
        return propertyId;
    }

    public void setPropertyId(String propertyId) {
        this.propertyId = propertyId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSortOnWebsite() {
        return sortOnWebsite;
    }

    public void setSortOnWebsite(String sortOnWebsite) {
        this.sortOnWebsite = sortOnWebsite;
    }

    public String getSortOnExpose() {
        return sortOnExpose;
    }

    public void setSortOnExpose(String sortOnExpose) {
        this.sortOnExpose = sortOnExpose;
    }

    public String getSortOnPortal() {
        return sortOnPortal;
    }

    public void setSortOnPortal(String sortOnPortal) {
        this.sortOnPortal = sortOnPortal;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getPortal() {
        return portal;
    }

    public void setPortal(String portal) {
        this.portal = portal;
    }

    public String getExpose() {
        return expose;
    }

    public void setExpose(String expose) {
        this.expose = expose;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public Image() {
    }

    @Override
    public String getExportFileName() {
        return "images";
    }

    @Override
    public String[] getHeaderNames() {
        return new String[]{
                "PropertyId",
                "Url",
                "FileName",
                "Title",
                "SortOnWebsite",
                "SortOnPortal",
                "SortOnExpose",
                "Website",
                "Portal",
                "Expose",
                "Tags"
        };
    }

}
