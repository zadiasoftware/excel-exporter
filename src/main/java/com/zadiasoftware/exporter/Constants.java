package com.zadiasoftware.exporter;

public interface Constants {

    interface OUTPUT {
        String FILE_LOCATION = "/tmp/exports";
        String URL_IMAGE_LOCATION = "http://zadiasoftware.com/estela";
        String FOLDER_LOCATION = "/Users/pablo/Documents/PropertyBase/finish";
    }

    interface IN {
        String DIR_LOCATION = "/Users/pablo/Documents/PropertyBase/properties";
    }

}
