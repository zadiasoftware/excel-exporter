package com.zadiasoftware.exporter.exporters;

import com.zadiasoftware.exporter.Constants;
import com.zadiasoftware.exporter.models.IdsEstela;
import com.zadiasoftware.exporter.models.Image;
import com.zadiasoftware.exporter.utils.file.FileCreator;
import org.apache.commons.io.FilenameUtils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.regex.Pattern;

public class ImageExporter {

    private String propertyRef = null;
    private String propertyID = null;

    private final Pattern NONLATIN = Pattern.compile("[^\\w-]");
    private final Pattern WHITESPACE = Pattern.compile("[\\s]");


    public List<Image> getAllImages() {
        File dir = new File(Constants.IN.DIR_LOCATION);
        List<Image> images = new ArrayList<>();
        if (dir.isDirectory()) {
            File[] files = dir.listFiles();
            if (files != null) {
                for (File f : files) {
                    if (f.isDirectory()) {
                        propertyRef = f.getName();
                        System.out.println("------ PROPIEDAD -> " + propertyRef + " ------");
                        propertyID = IdsEstela.properties.get(propertyRef);
                        if(propertyID != null && !propertyID.isEmpty()) {
                            if (f.isDirectory()) {
                                List<Image> imgs = getAllImagesInside(f, "Fotos");
                                for (Image img : imgs) {
                                    System.out.println("\t" + img.getUrl());
                                }
                                images.addAll(imgs);
                            }
                        }
                    }
                }
            }
        } else {
            System.out.println("Error, el directorio " + dir.getPath() + " está vacío");
        }

        return images;
    }


    private List<Image> getAllImagesInside(File f, String nextLevel) {

        if (f.isDirectory()) {
            File[] files = f.listFiles();
            if (files != null) {
                for (File fl : files) {
                    if (fl.getName().equalsIgnoreCase(nextLevel)) {
                        if (nextLevel.equalsIgnoreCase("web")) { // Llegamos a la carpeta de las imágenes
                            return generateImages(fl); // pasamos la carpeta web
                        }
                        return getAllImagesInside(fl, "web");
                    }
                }
            } else {
                System.out.println("Error, no hay ficheros en " + f.getPath());
            }
        } else {
            System.out.println("Error, " + f.getPath() + " no es un directorio");
        }

        return new ArrayList<>();
    }

    private List<Image> generateImages(File f) {
        FileCreator.createFolderImages(propertyRef);
        List<Image> result = new ArrayList<>();
        File[] listFiles = f.listFiles();
        if (listFiles != null) {
            for (File fl : listFiles) {
                if (!fl.isDirectory() && !FilenameUtils.getBaseName(fl.getName()).isEmpty()) {
                    Image img = new Image();
                    img.setPropertyId(propertyID);
                    String baseName = toNormalize(FilenameUtils.getBaseName(fl.getName()));
                    String name = baseName.concat(".").concat(FilenameUtils.getExtension(fl.getName()));
                    img.setUrl(Constants.OUTPUT.URL_IMAGE_LOCATION + "/" + propertyRef + "/images/" + name);
                    img.setFileName(name);
                    img.setTitle(FilenameUtils.getBaseName(baseName));
                    img.setSortOnWebsite("1");
                    img.setSortOnPortal("1");
                    img.setSortOnExpose("1");
                    img.setWebsite("TRUE");
                    img.setPortal("TRUE");
                    img.setExpose("TRUE");
                    img.setTags("");
                    result.add(img);
                    try { // copiar imagen a nueva carpeta
                        Files.copy(fl.toPath(), new File(Constants.OUTPUT.FOLDER_LOCATION + "/" + propertyRef + "/images/" + name).toPath(), StandardCopyOption.REPLACE_EXISTING);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

            }
            return result;
        } else {
            System.out.println("Error, no hay ficheros en " + f.getPath());
        }
        return new ArrayList<>();
    }

    private String toNormalize(String input) {
        String noWhiteSpace = WHITESPACE.matcher(input).replaceAll("-");
        String normalized = Normalizer.normalize(noWhiteSpace, Normalizer.Form.NFD);
        String slug = NONLATIN.matcher(normalized).replaceAll("");
        return slug.toLowerCase(Locale.ENGLISH);
    }

}
