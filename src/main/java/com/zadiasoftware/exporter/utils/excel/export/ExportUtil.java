package com.zadiasoftware.exporter.utils.excel.export;

import com.zadiasoftware.exporter.utils.file.FileCreator;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.lang.reflect.Field;
import java.util.List;

public class ExportUtil {

    public static File export(List<? extends Exportable> exportables) {
        try {
            if (exportables != null && exportables.size() > 0) {
                String name = exportables.get(0).getExportFileName() + ".xlsx";
                File file = new File(FileCreator.getExportsFolder(), name);
                FileOutputStream fos = new FileOutputStream(file);

                XSSFWorkbook builder = new XSSFWorkbook();
                Sheet sheet = builder.createSheet("Page 1");

                createHeader(sheet, exportables.get(0));
                for (int i = 0; i < exportables.size(); i++) {
                    createRow(builder, sheet, i + 1, exportables.get(i));
                }
                builder.write(fos);
                fos.close();

                return file;
            }
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static void createHeader(Sheet sheet, Exportable exportable) {
        try {
            Row row = sheet.createRow(0);
            for (int i = 0; i < exportable.getHeaderNames().length; i++) {
                Cell cell = row.createCell(i);
                cell.setCellValue(exportable.getHeaderNames()[i]);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void createRow(Workbook workbook, Sheet sheet, int index, Object exportable) {
        try {
            Row row = sheet.createRow(index);
            Field[] fields = exportable.getClass().getDeclaredFields();
            for (int i = 0; i < fields.length; i++) {
                Cell cell = row.createCell(i);
                Field field = fields[i];
                field.setAccessible(true);
                Object value = field.get(exportable);
                cell.setCellValue(value != null ? "'" + String.valueOf(value) + "'" : "\'\'");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
