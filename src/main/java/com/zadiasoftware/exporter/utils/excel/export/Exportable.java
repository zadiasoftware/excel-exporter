package com.zadiasoftware.exporter.utils.excel.export;

public interface Exportable {

    String getExportFileName();

    String[] getHeaderNames();

}
