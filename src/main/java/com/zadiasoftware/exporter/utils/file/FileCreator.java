package com.zadiasoftware.exporter.utils.file;

import com.zadiasoftware.exporter.Constants;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

public class FileCreator {

    public static void assertFolderExists() {
        File file = new File(Constants.OUTPUT.FILE_LOCATION);
        if (!file.exists()) {
            if (!file.mkdirs()) {
                throw new RuntimeException("There was not possible to create the asset folder. Permission Denied");
            }
        }
    }

    public static void createFolderImages(String propertyRef) {
        File file = new File(Constants.OUTPUT.FOLDER_LOCATION + "/" + propertyRef + "/images/");
        if (!file.exists()) {
            if (!file.mkdirs()) {
                throw new RuntimeException("There was not possible to create the images folder. Permission Denied");
            }
        }
    }

    public static File getExportsFolder() {
        assertFolderExists();
        File file = new File(Constants.OUTPUT.FILE_LOCATION);
        if (!file.exists()) {
            if (!file.mkdirs()) {
                throw new RuntimeException("There was not possible to create the exports folder. Permission Denied");
            }
        }
        return file;
    }

}
